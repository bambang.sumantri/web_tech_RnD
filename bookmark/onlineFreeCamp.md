## Online code camp
---
*why?* : Online camp have curriculum, which make thing easier to understand.

*why?* : It break things to smaller thing, and order it appropriately.

---
### [FreeCodeCamp](http://freecodecamp.com/ "FCC website")
---
*why?* : It's free, and knowledge should free.

*why?* : It'll give you certification when you complete the challenges.

*why?* : It have [gitter](http://gitter.am Gitter website), A nice place to ask when you stuck.

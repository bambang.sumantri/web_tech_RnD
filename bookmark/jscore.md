## Fundamental javascript knowledge you should know
---
Initial text
`banal` should have `table of content`.

[First to read](https://github.com/benoror/ama/issues/1 "Behonor ama")

### Table of Contents
---
  0. [javascript style guide](https://github.com/airbnb/javascript "Airbnb style guide")
  0. [Optimization killers](https://github.com/petkaantonov/bluebird/wiki/Optimization-killers#3-managing-arguments "github wiki")
  0. [Principles of Writing Consistent, Idiomatic JavaScript](https://github.com/rwaldron/idiomatic.js "idiomatic.js repo")
  0. [What is the DOM?](https://css-tricks.com/dom/ "css-tricks atricle")
  0. [Intro to DOM](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction "MDN")
  0. [How to CORS](http://www.eriwen.com/javascript/how-to-cors/)
  0. [CORS MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS)

### Tools
  0. [ESlint](http://eslint.org "eslint website")
